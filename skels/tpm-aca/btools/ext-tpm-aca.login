#!/bin/sh

export PATH=$PATH:/usr/local/bin:/sbin

getvaluefromconfig() {
	key=$1
	value=`cat /storage/config/pantahub.config | grep ${key}= | sed -e 's/.*'${key}'=//;s/ .*//'`
	echo $value
}

function clean() {
	if mount | grep /chroot/proc > /dev/null; then
		umount /chroot/proc
	fi

	if mount | grep /chroot/dev > /dev/null; then
		umount /chroot/dev
	fi

	if mount | grep /chroot/tmp > /dev/null; then
		umount /chroot/tmp
	fi

	if mount | grep /chroot/config > /dev/null; then
		umount /chroot/config
	fi

	if mount | grep /chroot/config > /dev/null; then
		umount /chroot
	fi
}

if [ "$DEBUG" == true ]; then set -ex; else set -e; fi

cmd=$0
type=`getvaluefromconfig creds.type`
deviceid=`getvaluefromconfig creds.id`

if [ "$type" != "ext-tpm-aca" ]; then
	echo "The pantahub.config file doesn't support login with the tpm-aca flow"
	exit 1
fi

if [ -z "$deviceid" ]; then
	echo "The device is not register yet, please register the device first"
	exit 0
fi

if [ ! -d "/chroot" ]; then
	mkdir /chroot
fi

clean || true
sleep 2

mount /btools/toolbox.ext-tpm-aca.squashfs /chroot
mount -t proc proc /chroot/proc
mount -t devtmpfs dev /chroot/dev
mount -o bind /tmp /chroot/tmp
mount -o bind /storage/config /chroot/config

sleep 2
pv_x509_host=`cat /pv/pantahub-host | sed -e 's/https:\/\//https:\/\/x509\./'`
chroot /chroot pv-handler-login $pv_x509_host /config/pantahub.config
sleep 2

clean
sync

exit 0
